package com.backspace119.whackamole.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Skull;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.backspace119.whackamole.WAM;
import com.backspace119.whackamole.utils.exceptions.MalformedConfigException;
import com.backspace119.whackamole.utils.handlers.ConfigHandler;
import com.backspace119.whackamole.utils.handlers.HighscoresHandler;

/**
 * This is intended to handle all the utilty and static methods between games. It provides rules that
 * remain constant between games as well.
 * @author backspace119
 *
 */
public class GamesManager {
	
	private GamesManager()
	{
		
	}
	private static ConfigHandler wConfigH = new ConfigHandler((JavaPlugin) WAM.getInstance(), "wool.yml", true);
	private static Map<String, List<Location>> highscoreSignMap = new HashMap<String, List<Location>>();
	private static ConfigHandler gConfigH = new ConfigHandler((JavaPlugin) WAM.getInstance(), "games.yml", true);
	private static Map<String, Game> gameMap = new HashMap<String, Game>();
	private static List<UUID> players = new ArrayList<UUID>();
	private static Map<UUID, Game> playerGameMap = new HashMap<UUID, Game>(); 
	private static Map<String, List<Location>> signsMap = new HashMap<String, List<Location>>();
	private static ConfigHandler sConfigH = new ConfigHandler((JavaPlugin) WAM.getInstance(), "wam.yml", true);
	public static Game getGame(String name)
	{
		return gameMap.get(name);
	}
	
	
	
	
	public static int getWoolPoints(DyeColor color)
	{
		return wConfigH.getConfig().getInt("wool." + color.name());
	}
	public static int getMoleScarcity() throws MalformedConfigException
	{
		if(sConfigH.getConfig().getInt("moleScarcity") <= 0)
		{
			throw new MalformedConfigException("moleScarcity MUST BE GREATER THAN 0. PLEASE CHANGE THE VALUE IN wam.yml");
		}
		return sConfigH.getConfig().getInt("moleScarcity");
	}
	public static double getMoleTimeDivisor() throws MalformedConfigException
	{
		if(sConfigH.getConfig().getInt("moleTimeDivisor") <= 0)
		{
			throw new MalformedConfigException("moleTimeDivisor MUST BE GREATER THAN 0. PLEASE CHANGE THE VALUE IN wam.yml");
		}
		return sConfigH.getConfig().getDouble("moleTimeDivisor");
	}
	
	
	public static Map<String, List<Location>>  getHighscoreSignMap()
	{
		return highscoreSignMap;
	}
	
	
	public static void saveGame(Location seed, String name, double cost, double seconds, long jackpot)
	{
		gConfigH.getConfig().set(name + ".x", seed.getBlockX());
		gConfigH.getConfig().set(name + ".y", seed.getBlockY());
		gConfigH.getConfig().set(name + ".z", seed.getBlockZ());
		gConfigH.getConfig().set(name + ".w", seed.getWorld().getName());
		gConfigH.getConfig().set(name + ".c", cost);
		gConfigH.getConfig().set(name + ".s", seconds);
		gConfigH.getConfig().set(name + ".j", jackpot);
		
		gConfigH.saveConfig();
	}
	public static void saveJackpot(String game, long j)
	{
		gConfigH.getConfig().set(game + ".j", j);
		gConfigH.saveConfig();
	}
	public static void reloadGames()
	{
		for(String key: gConfigH.getConfig().getKeys(false))
		{
			loadGame(key);
		}
		HighscoresHandler.loadHighScores();
	}
	public static void newHighscoreSign(String game,Location l)
	{
		List<Location> signs;
		if(getHighscoreSignsForGame(game) != null)
		{
			signs = getHighscoreSignsForGame(game);
		}else{
			signs = new ArrayList<Location>();
		}
		
		signs.add(l);
		highscoreSignMap.put(game, signs);
		saveHighscoreSigns(game, signs);
		
	}
	/**
	 * block must already be set to a skull material
	 * @param l
	 * @param playerName
	 */
	public static void setSkullSkin(Location l, String playerName)
	{
		 Block block = l.getBlock();
	       
		 BlockState blockState = block.getState();
		  
		 if (blockState instanceof Skull)
		 {
		     Skull skull = (Skull) blockState;
		     skull.setOwner(playerName);
		     skull.update();
		 } 
	}
	public static void saveHeads(String game)
	{
		gConfigH.getConfig().set(game + ".heads", "");
		for(String head:getGame(game).getHeadsList())
		{
			gConfigH.getConfig().set(game + ".heads." + head + ".p", getGame(game).getHeadsPoints().get(head));
			gConfigH.getConfig().set(game + ".heads." + head + ".r", getGame(game).getHeadsRarity().get(head));
		}
		gConfigH.saveConfig();
	
	}
	public static void saveHighscoreSigns(String game, List<Location> signs)
	{
		gConfigH.getConfig().set(game + ".hsigns", "");
		int i = 0;
		for(Location s: signs)
		{
			gConfigH.getConfig().set(game + ".hsigns." + i + ".x", s.getBlockX());
			gConfigH.getConfig().set(game + ".hsigns." + i + ".y", s.getBlockY());
			gConfigH.getConfig().set(game + ".hsigns." + i + ".z", s.getBlockZ());
			gConfigH.getConfig().set(game + ".hsigns." + i + ".w", s.getWorld().getName());
			i++;
		}
	}
	private static Game loadGame(final String name)
	{
		Location l = new Location(WAM.getInstance().getServer().getWorld(gConfigH.getConfig().getString(name + ".w")), gConfigH.getConfig().getInt(name + ".x"), gConfigH.getConfig().getInt(name + ".y"), gConfigH.getConfig().getInt(name + ".z"));
		
		if(gConfigH.getConfig().get(name + ".g") == null)
		{
			gConfigH.getConfig().set(name + ".g", true);
			gConfigH.saveConfig();
		}
		List<Location> signs = new ArrayList<Location>();
		if(gConfigH.getConfig().getConfigurationSection(name + ".hsigns") == null)
		{
			gConfigH.getConfig().set(name + ".hsigns", "empty");
		}
		if(!gConfigH.getConfig().getString(name + ".hsigns").equals("empty"))
		{
		for(String key: gConfigH.getConfig().getConfigurationSection(name + ".hsigns").getKeys(false))
		{
			key = name + ".hsigns." + key;
			Location s = new Location(WAM.getInstance().getServer().getWorld(gConfigH.getConfig().getString(key + ".w")), gConfigH.getConfig().getInt(key + ".x"), gConfigH.getConfig().getInt(key + ".y"), gConfigH.getConfig().getInt(key + ".z"));
			signs.add(s);
		}
		
		
		setHighscoreSignsForGame(name, signs);
		}
		if(!gConfigH.getConfig().getBoolean(name + ".g"))
		{
			if(gConfigH.getConfig().getConfigurationSection(name + ".heads").getKeys(false) != null)
			{
				List<String> heads = new ArrayList<String>();
				Map<String, Integer> headPoints = new HashMap<String, Integer>();
				Map<String, Integer> headRarity = new HashMap<String, Integer>();
				for(String head:gConfigH.getConfig().getConfigurationSection(name + ".heads").getKeys(false))
				{
					heads.add(head);
					headPoints.put(head, gConfigH.getConfig().getInt(name + ".heads." + head + ".p"));
					headRarity.put(head, gConfigH.getConfig().getInt(name + ".heads." + head + ".r"));
					return new Game(name, prepareMoles(l), l.getWorld().getName(), gConfigH.getConfig().getDouble(name + ".s"), gConfigH.getConfig().getDouble(name  + ".c"), gConfigH.getConfig().getBoolean(name + ".g"), heads, headPoints, headRarity).setJackpot(gConfigH.getConfig().getLong(name + ".j"));
				}
			}
		}
		return new Game(name,prepareMoles(l) ,l.getWorld().getName(), gConfigH.getConfig().getDouble(name + ".s"), gConfigH.getConfig().getDouble(name + ".c"), gConfigH.getConfig().getBoolean(name + ".g")).setJackpot(gConfigH.getConfig().getLong(name + ".j"));
	}
	
	
	
	
	
	public static void addGame(Game game)
	{
		gameMap.put(game.getName(), game);
	}
	public static Game getGameByPlayer(UUID id)
	{
		return playerGameMap.get(id);
	}
	
	public static FileConfiguration getGamesFile()
	{
		return gConfigH.getConfig();
	}
	
	public static boolean isPlaying(UUID id)
	{
		return players.contains(id);
	}
	
	public static void addPlayer(UUID id, Game game)
	{
		players.add(id);
		playerGameMap.put(id, game);
	}
	
	public static void removePlayer(UUID id)
	{
		players.remove(id);
	}

public static List<Location> getHighscoreSignsForGame(String name) {
		
		return highscoreSignMap.get(name);
	}

	public static void setHighscoreSignsForGame(String name, List<Location> signs) {
		highscoreSignMap.put(name, signs);
	}
	public static List<Location> getSignsForGame(String name) {
		
		return signsMap.get(name);
	}

	public static void setSignsForGame(String name, List<Location> signs) {
		signsMap.put(name, signs);
	}
	
	public static List<Location> prepareMoles(Location seed)
	{
		List<Location> moles = new ArrayList<Location>();
		
		World world = seed.getWorld();
		moles.add(seed);
		
		moles.add(new Location(world, seed.getBlockX() + 1, seed.getBlockY(), seed.getBlockZ() - 1));
		moles.add(new Location(world, seed.getBlockX() + 1, seed.getBlockY(), seed.getBlockZ() + 1));
		moles.add(new Location(world, seed.getBlockX() + 1, seed.getBlockY(), seed.getBlockZ()));
		moles.add(new Location(world, seed.getBlockX() - 1, seed.getBlockY(), seed.getBlockZ() - 1));
		moles.add(new Location(world, seed.getBlockX() - 1, seed.getBlockY(), seed.getBlockZ() + 1));
		moles.add(new Location(world, seed.getBlockX() - 1, seed.getBlockY(), seed.getBlockZ()));
		moles.add(new Location(world, seed.getBlockX(), seed.getBlockY(), seed.getBlockZ() + 1));
		moles.add(new Location(world, seed.getBlockX(), seed.getBlockY(), seed.getBlockZ() - 1));
		
		return moles;
	}
	public static void removeHighscoreSignForGame(String game, Location s) {
		
		if(getHighscoreSignsForGame(game) == null || getHighscoreSignsForGame(game).isEmpty())
		{
			return;
		}
		List<Location> signs = getHighscoreSignsForGame(game);
		signs.remove(s);
		highscoreSignMap.put(game, signs);
		saveHighscoreSigns(game, signs);
		
	}
	
}
