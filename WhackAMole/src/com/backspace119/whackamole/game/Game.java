package com.backspace119.whackamole.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.material.Wool;
import org.bukkit.scoreboard.Score;

import com.backspace119.whackamole.WAM;
import com.backspace119.whackamole.utils.exceptions.MalformedConfigException;
import com.backspace119.whackamole.utils.handlers.GUIManager;
import com.backspace119.whackamole.utils.handlers.HighscoresHandler;
import com.backspace119.whackamole.utils.handlers.GUIManager.MessageType;

/**
 * A bucket for some logic and variables that run per game. Anything that is not per-game 
 * will be found in the GamesManager class
 * @author backspace119
 *
 */
public class Game {

	
	Score pscore;
	double divisor;
	int moleScarcity;
	long jackpot = 0;
	Server server;
	List<Block> moles = new ArrayList<Block>();
	List<Location> moleLocs;
	int tickPasses = 0;
	UUID id;
	long points = 0;
	String world;
	String name;
	boolean running = false;
	double cost = 0;
	boolean woolGame;
	List<String> heads = new ArrayList<String>();
	Map<String, Integer> headPoints = new HashMap<String, Integer>();
	Map<String, Integer> headRarity = new HashMap<String, Integer>();
	//misleading name, it decreases entropy all together by making block appearances less or more rare
	//depending on their value
	int entropyS1 = 0;
	public Game(String name,List<Location> moleLocs, String world, double seconds, double cost, boolean woolGame)
	{
		server = WAM.getInstance().getServer();
		this.moleLocs = moleLocs;
		this.name = name;
		this.world = world;
		this.tickPasses = (int) seconds * 20;
		GamesManager.addGame(this);
		this.cost = cost;
		this.woolGame = woolGame;
		setup();
	}
	/**
	 * for use with games that use mob/player heads 
	 * instead of wool. the woolGame boolean is redundant
	 * and should be phased out.
	 * @param name
	 * @param moleLocs
	 * @param world
	 * @param seconds
	 * @param cost
	 * @param woolGame
	 * @param heads
	 * @param headPoints
	 * @param headRarity
	 */
	public Game(String name, List<Location> moleLocs, String world, double seconds, double cost, boolean woolGame, List<String> heads, Map<String, Integer> headPoints, Map<String, Integer> headRarity)
	{
		this.heads = heads;
		this.headPoints = headPoints;
		this.headRarity = headRarity;
		new Game(name, moleLocs, world, seconds, cost, woolGame);
	}
	
	/**
	 * this returns a game so that i can use it at startup to go ahead and set the jackpot
	 * the game is just an instance of the game used for this method so it is
	 * fairly pointless to collect it for other things
	 * @param jackpot
	 * @return
	 */
	public Game setJackpot(long jackpot)
	{
		this.jackpot = jackpot;
		return this;
	}
	public List<String> getHeadsList()
	{
		return heads;
	}
	public Map<String, Integer> getHeadsPoints()
	{
		return headPoints;
	}
	public Map<String, Integer> getHeadsRarity()
	{
		return headRarity;
	}
	public void addHead(String name, int points, int rarity)
	{
		if(!heads.contains(name))
		{
			heads.add(name);
			headPoints.put(name, points);
			headRarity.put(name, rarity);
		}
		
	}
	public String getName()
	{
		return name;
	}
	public void setup()
	{
		try{
			divisor = GamesManager.getMoleTimeDivisor();
			} catch (MalformedConfigException e)
			{
				e.printStackTrace();
				divisor = 1;
			}
		
		try{
			moleScarcity = GamesManager.getMoleScarcity();
		}catch(MalformedConfigException e)
		{
			e.printStackTrace();
			moleScarcity = 500;
		}
	}
	public void joinGame(UUID id)
	{
		setup();
		jackpot =  jackpot + (long) cost;
		WAM.getEcon().withdrawPlayer(server.getPlayer(id).getName(), cost);
		if(jackpot > (10* (cost + entropyS1)))
		{
			entropyS1++;
		}
		
		if(jackpot > 1000 * cost)
		{
			LinkedList<Long> scores = HighscoresHandler.getHighscoresForGame(name);
			int index = 0;
			for(Long l: scores)
			{
				scores.set(index, l/2);
				index++;
			}
			HighscoresHandler.setHighscoresForGame(name,scores);
		}
		
		running = true;
		this.id = id;
		setupScoreboard();
		GamesManager.addPlayer(id, this);
		gameLoop(0);
	}
	public boolean hasMole(Location l)
	{
		return moleLocs.contains(l);
	}
	public int getTime()
	{
		return tickPasses/20;
	}
	DyeColor dye = DyeColor.BLACK;
	int length = 0;
	Random rand = new Random();
	public void gameLoop(final int ticks)
	{
		if(tickPasses <= ticks)
		{
			endGame();
			return;
		}
		dye = null;
		final Location roundLoc = moleLocs.get(rand.nextInt(moleLocs.size()));
		//let's hope this giant block doesn't hold up past a tick....then
		//our timing would be off
		if(woolGame)
		{
		if(rand.nextInt(moleScarcity) < 90 - entropyS1)
		{
			dye = DyeColor.BLACK;
			length = 25;
		}
		if(rand.nextInt(moleScarcity) < 85 - entropyS1)
		{
			dye = DyeColor.BLUE;
			length = 24;
		}
		if(rand.nextInt(moleScarcity) < 80 - entropyS1)
		{
			dye = DyeColor.BROWN;
			length = 23;
		}
		if(rand.nextInt(moleScarcity) < 75 - entropyS1)
		{
			dye = DyeColor.CYAN;
			length = 22;
		}
		if(rand.nextInt(moleScarcity) < 70 - entropyS1)
		{
			dye = DyeColor.GRAY;
			length = 21;
		}
		if(rand.nextInt(moleScarcity) < 65 - entropyS1)
		{
			dye = DyeColor.GREEN;
			length = 20;
		}
		if(rand.nextInt(moleScarcity) < 60 - entropyS1)
		{
			dye = DyeColor.LIGHT_BLUE;
			length = 19;
		}
		if(rand.nextInt(moleScarcity) < 55 - entropyS1)
		{
			dye = DyeColor.LIME;
			length = 18;
		}
		if(rand.nextInt(moleScarcity) < 50 - entropyS1)
		{
			dye = DyeColor.MAGENTA;
			length = 17;
		}
		if(rand.nextInt(moleScarcity) < 45 - entropyS1)
		{
			dye = DyeColor.ORANGE;
			length = 16;
		}
		if(rand.nextInt(moleScarcity) < 40 + entropyS1)
		{
			dye = DyeColor.PINK;
			length = 15;
		}
		if(rand.nextInt(moleScarcity) < 35 + entropyS1)
		{
			dye = DyeColor.PURPLE;
			length = 14;
		}
		if(rand.nextInt(moleScarcity) < 30 + entropyS1)
		{
			dye = DyeColor.RED;
			length = 13;
		}
		if(rand.nextInt(moleScarcity) < 25 + entropyS1)
		{
			dye = DyeColor.SILVER;
			length = 12;
		}
		if(rand.nextInt(moleScarcity) < 20 + entropyS1)
		{
			dye = DyeColor.WHITE;
			length = 11;
		}
		if(rand.nextInt(moleScarcity) < 15 + entropyS1)
		{
			dye = DyeColor.YELLOW;
			length = 10;
		} 
		
		if(dye != null)
		{
		addMole(roundLoc, dye, length, Material.WOOL);
		}
		}else{
			String name = "";
			
			for(String head: heads)
			{
				if(rand.nextInt(headRarity.get(head))< 100)
				{
					name = head;
					if(rand.nextBoolean())
					{
						break;
					}
				}
			}
			
			if(name != "")
			{
			addMole(roundLoc, name, 20, Material.SKULL);
			}else{
				addMole(roundLoc, SkullType.values()[rand.nextInt(SkullType.values().length)], 25, Material.SKULL);
			}
			
		}
		
		server.getScheduler().scheduleSyncDelayedTask(WAM.getInstance(), new Runnable(){
			@Override
			public void run()
			{
				gameLoop(ticks + 1);
			}
		}, 1L);
		
		
	}
	public void woolClicked(Wool wool, Block block)
	{
		if(moles.contains(block))
		{	
			server.getPlayer(id).playSound(server.getPlayer(id).getLocation(), Sound.DONKEY_DEATH, 1, 1);
		points = points + GamesManager.getWoolPoints(wool.getColor());;
		//server.getPlayer(id).sendMessage(ChatColor.GREEN + "You got one! " + getPointAssignment(wool.getColor()) + " points");
		pscore.setScore((int) points);
		moles.remove(block);
		server.getWorld(world).getBlockAt(block.getLocation()).setType(Material.AIR);
		
		
		}
	}
	public long getJackpot()
	{
		return jackpot;
	}
	public double getCost()
	{
		return cost;
	}
	
	public void setupScoreboard()
	{
		List<String> s = new ArrayList<String>();
		s.add("Your Score: ");
		Map<String, Score> scores = GUIManager.wrapHUD("WAM", s, MessageType.POS_I, server.getPlayer(id));
		
		for(String a: scores.keySet())
		{
			pscore = scores.get(a);
		}
		
	}
	
	private void endGame()
	{
		GUIManager.stopHUD(server.getPlayer(id));
		server.getPlayer(id).sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Time's up! You got: " + points + " points!");
		int highscoreSpot = HighscoresHandler.checkHighscores(name, server.getPlayer(id).getName(), points);
		if(highscoreSpot == 0)
		{
			server.getPlayer(id).sendMessage(ChatColor.GREEN + "YOU BEAT THE HIGHSCORE!! YOU WIN THE JACKPOT FOR THIS GAME: " + jackpot + " " + WAM.getEcon().currencyNamePlural());
			WAM.getEcon().depositPlayer(server.getPlayer(id).getName(), jackpot);
			jackpot = 0;
		}else{
			
			server.getPlayer(id).sendMessage(ChatColor.YELLOW + "You didn't beat the highscore, it remains at: " + HighscoresHandler.getHighscoresForGame(name).get(0) + " and jackpot is now: " + jackpot + " " + WAM.getEcon().currencyNamePlural());
		}
		
		if(highscoreSpot != -1)
		{
			server.getPlayer(id).sendMessage(ChatColor.YELLOW + "You made the highscore board! Do /wam highscores <game> to see your placement");
		}
		moles.clear();
		points = 0;
		running = false;
		moles.clear();
		GamesManager.removePlayer(id);
		GamesManager.saveJackpot(name, jackpot);
		updateSigns();
		
		
	}
	
	public void updateSigns()
	{
		for(Location l: GamesManager.getSignsForGame(name))
		{
			Sign sign = (Sign) server.getWorld(l.getWorld().getName()).getBlockAt(l).getState();
			sign.setLine(1, ChatColor.GREEN + "Jackpot: " + jackpot);
			sign.update();
		}
		if(GamesManager.getHighscoreSignsForGame(name) != null)
		{
		for(Location l: GamesManager.getHighscoreSignsForGame(name))
		{
			Sign sign = (Sign) server.getWorld(l.getWorld().getName()).getBlockAt(l).getState();
			sign.setLine(3, ChatColor.GREEN + String.valueOf( HighscoresHandler.getHighscore(name)));
			sign.update();
		}
		}
	}
	public boolean isRunning()
	{
		return running;
	}
	public void headClicked(Skull skull)
	{
		if(moles.contains(skull))
		{
			server.getPlayer(id).playSound(server.getPlayer(id).getLocation(), Sound.BAT_HURT, 1, 1);
			points = points + headPoints.get(skull.getOwner());
			//server.getPlayer(id).sendMessage(ChatColor.GREEN + "You got one! " + getPointAssignment(wool.getColor()) + " points");
			pscore.setScore((int) points);
			moles.remove(skull);
			server.getWorld(world).getBlockAt(skull.getLocation()).setType(Material.AIR);
			
		}
	}
	@SuppressWarnings("deprecation")
	private void addMole(final Location roundLoc, Object data, long ticks, Material m)
	{
		DyeColor color = null;
		
		if(data instanceof DyeColor)
		{
			color = (DyeColor) data;
		}
		
		if(m.equals(Material.SKULL) && !(data instanceof SkullType))
		{
			if(!(data instanceof String))
			{
				data = "Notch";
			}
		}
		if(server.getWorld(world).getBlockAt(roundLoc).getType().equals(Material.AIR))
		{
		
			
		server.getWorld(world).getBlockAt(roundLoc).setType(m);
		if(color != null)
		{
		server.getWorld(world).getBlockAt(roundLoc).setData(color.getData());
		}else if(m.equals(Material.SKULL) && !(data instanceof SkullType))
		{
			GamesManager.setSkullSkin(roundLoc, (String) data);
		}else{
			SkullType type = (SkullType) data;
			
			server.getWorld(world).getBlockAt(roundLoc).setData((byte)type.ordinal());
		}
		//need to rotate head to face player if the block is indeed a head here
		
		server.getScheduler().scheduleSyncDelayedTask(WAM.getInstance(), new Runnable(){
			@Override
			public void run()
			{
				//moles.remove(server.getWorld(world).getBlockAt(roundLoc));
				server.getWorld(world).getBlockAt(roundLoc).setType(Material.AIR);
				
				 
			}
		},(long) (ticks/divisor) );
		moles.add(server.getWorld(world).getBlockAt(roundLoc));
		}
	}
}
