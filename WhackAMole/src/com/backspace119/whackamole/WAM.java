package com.backspace119.whackamole;

import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.ChatColor;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.backspace119.whackamole.events.PlayerEvents;
import com.backspace119.whackamole.game.GamesManager;
import com.backspace119.whackamole.utils.handlers.CommandHandler;
import com.backspace119.whackamole.utils.handlers.GUIManager;

public class WAM extends JavaPlugin{

	static Economy econ;
	static final Logger logger = Logger.getLogger("Minecraft");
	static final String prefix = "[WAM]";
	static WAM plugin;
	static boolean hasEconomy;
	public static void consoleOut(String message)
	{
		logger.info(prefix + " " + message);
	}
	
	public static Economy getEcon()
	{
		return econ;
	}

	@Override
	public void onEnable()
	{
		hasEconomy = setupEconomy();
		
		if(!hasEconomy)
		{
			consoleOut(ChatColor.RED + "NO ECONOMY PLUGIN FOUND! WAM IS SHUTTING DOWN!");
			getServer().getPluginManager().disablePlugin(this);
			return;
			
		}
		plugin = this;
		getCommand("wam").setExecutor(new CommandHandler());
		getServer().getPluginManager().registerEvents(new PlayerEvents(), this);
		GamesManager.reloadGames();
		GUIManager.init();
	}
	
	public static WAM getInstance()
	{
		return plugin;
	}
	
	
	/**
	 * checks if the Vault plugin is installed. If it is it will return true and
	 * the global Economy variable will be set to true
	 * 
	 * @return true if Vault exists
	 */
	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			logger.severe("VAULT NOT FOUND!");
			return false;
		}

		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (rsp == null) {
			logger.severe("REGISTERED SERVICE PROVIDER DID NOT FIND VAULT!");
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}
}
