/**
 * 
 * @author backspace119
 *
 *
 *Copyright (C) 2013  backspace119 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *
 */
package com.backspace119.whackamole.utils.handlers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * This is a generic yml config handler used across my plugins you may use it
 * when porting into my plugin for your own configs but it is designed to work
 * for mine
 * 
 */
public class ConfigHandler {

	private static List<ConfigHandler> configs = new ArrayList<ConfigHandler>();
	private final String fileName;
	private FileConfiguration fileConfiguration = null;
	private File configFile = null;
	private final JavaPlugin plugin;
	private final boolean saveToDisk;

	public ConfigHandler(JavaPlugin javaPlugin, String fileName, boolean saveToDisk) {
		if (javaPlugin == null) throw new IllegalArgumentException("plugin cannot be null");
		this.plugin = javaPlugin;
		this.fileName = fileName;
		File dataFolder = javaPlugin.getDataFolder();
		if (dataFolder == null) throw new IllegalStateException();
		this.configFile = new File(javaPlugin.getDataFolder(), fileName);
		this.saveToDisk = saveToDisk;
		configs.add(this);
	}

	public static void reloadConfigs()
	{
		for(ConfigHandler c: configs)
		{
			c.reloadConfig();
		}
	}
	public void reloadConfig() {
		fileConfiguration = YamlConfiguration.loadConfiguration(configFile);
		// Look for defaults in the jar
		InputStream defConfigStream = plugin.getResource(fileName);
		if (defConfigStream != null) {
			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
			fileConfiguration.setDefaults(defConfig);
		}
		if (!configFile.exists()) {
			saveDefaultConfig();
			reloadConfig();
		}
	}

	public FileConfiguration getConfig() {

		if (fileConfiguration == null) {
			plugin.getLogger().info("had to reload config");
			this.reloadConfig();
		}
		return fileConfiguration;
	}

	public void saveConfig() {
		if (saveToDisk) {
			if (fileConfiguration == null || configFile == null) {
				return;
			} else {
				try {
					getConfig().save(configFile);
				} catch (IOException ex) {
					plugin.getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
				}
			}
		}
	}

	public void saveDefaultConfig() {
		if (saveToDisk) {
			plugin.saveResource(fileName, true);
		}
	}

}
