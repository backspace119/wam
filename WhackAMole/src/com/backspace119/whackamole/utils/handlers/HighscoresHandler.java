package com.backspace119.whackamole.utils.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import com.backspace119.whackamole.WAM;

public class HighscoresHandler {

	private HighscoresHandler()
	{
		
	}
	private static final String HIGHSCORE = "highscore.";
	private static final String HIGHSCORER = "highscorer.";
	private static ConfigHandler hConfigH = new ConfigHandler((JavaPlugin) WAM.getInstance(), "highscores.yml", true);
	
	private static Map<String, LinkedList<Long>> highscoreMap = new HashMap<String, LinkedList<Long>>();
	private static Map<String, LinkedList<String>> highscorePlayerMap = new HashMap<String, LinkedList<String>>();
	public static Map<String, LinkedList<String>> getPlayerMap()
	{
		return highscorePlayerMap;
	}
	public static Map<String, LinkedList<Long>> getHighscoreMap()
	{
		return highscoreMap;
	}
	public static void newHighscoreList(String game)
	{
		LinkedList<Long> scores = new LinkedList<Long>();
		LinkedList<String> names = new LinkedList<String>();
		for(int i = 0; i < 5; i++)
		{
			scores.add(0L);
			names.add("none");
		}
		
		highscoreMap.put(game, scores);
		highscorePlayerMap.put(game, names);
		setAndSave(game);
		
	}
	public static LinkedList<Long> getHighscoresForGame(String game)
	{
		return highscoreMap.get(game);
	}
	public static Long getHighscore(String game)
	{
		return highscoreMap.get(game).get(0);
	}
	public static String getHighscorer(String game)
	{
		return highscorePlayerMap.get(game).get(0);
	}
	public static void setHighscoresForGame(String game, LinkedList<Long> scores)
	{
		highscoreMap.put(game, scores);
	}
	public static LinkedList<String> getHighscorersForGame(String game)
	{
		return highscorePlayerMap.get(game);
	}
	public static int checkHighscores(String game, String name, long score)
	{
		int index = 0;
		for(Long l:highscoreMap.get(game))
		{
			if(score > l)
			{
				highscoreMap.get(game).add(index, score);
				highscorePlayerMap.get(game).add(index, name);
				
				highscoreMap.get(game).removeLast();
				highscorePlayerMap.get(game).removeLast();
				setAndSave(game);
				return index;
			}
			index++;
		}
		
		return -1;
	}
	public static void loadHighScores()
	{
		LinkedList<String> names = new LinkedList<String>();
		LinkedList<Long> scores = new LinkedList<Long>();
		
		for(String game: hConfigH.getConfig().getConfigurationSection(HIGHSCORER).getKeys(false))
		{
			highscorePlayerMap.put(game, names);
			for(String name: hConfigH.getConfig().getStringList(formatRequest(HIGHSCORER, game)))
			{
				highscorePlayerMap.get(game).addLast(name);
			}
			highscoreMap.put(game, scores);
			for(Long score: hConfigH.getConfig().getLongList(formatRequest(HIGHSCORE, game)))
			{
				highscoreMap.get(game).addLast(score);
			}
			
			setAndSave(game);
		}
		
		
	}
	public static void setAndSave(String game)
	{
		
		while(highscorePlayerMap.get(game).size() > 5)
		{
			highscorePlayerMap.get(game).removeLast();
		}
		while(highscoreMap.get(game).size() > 5)
		{
			highscoreMap.get(game).removeLast();
		}
		List<String> names = new ArrayList<String>();
		for(String name:highscorePlayerMap.get(game))
		{
			names.add(name);
		}
		hConfigH.getConfig().set(formatRequest(HIGHSCORER, game), names);
		List<Long> scores = new ArrayList<Long>();
		for(Long score: highscoreMap.get(game))
		{
			scores.add(score);
		}
		hConfigH.getConfig().set(formatRequest(HIGHSCORE, game), scores);
		
		hConfigH.saveConfig();
	}
	
	public static String formatRequest(String prefix, String game)
	{
		return prefix + game;
		
	}
	
	public static String formatRequest(String prefix, String game, String suffix)
	{
		return prefix + game + "." + suffix;
	}
}
