package com.backspace119.whackamole.utils.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import com.backspace119.whackamole.WAM;
/**
 * a utility class to wrap player UI handling.
 * this standardizes messages by a like-grouping
 * organizational strategy
 * very straightforward
 * @author backspace119
 *
 */
public class GUIManager {

	private GUIManager()
	{
		
	}
	private static ChatColor im = ChatColor.BOLD;
	private static ChatColor pos = ChatColor.GREEN;
	private static ChatColor neg = ChatColor.GOLD;
	private static ChatColor mod = ChatColor.BLUE;
	static ScoreboardManager sbm;
	
	public static void init()
	{
		sbm = Bukkit.getScoreboardManager();
	}
	/**
	 * sets the ChatColor for all messages in the order:
	 * important
	 * positive
	 * negative
	 * moderate
	 */
	public static void setupAll(ChatColor im, ChatColor pos, ChatColor neg, ChatColor mod)
	{
		GUIManager.im = im;
		GUIManager.pos = pos;
		GUIManager.neg = neg;
		GUIManager.mod = mod;
	}
	/**
	 * set ChatColor for important messages
	 * @param im
	 */
	public static void setIm(ChatColor im) {
		GUIManager.im = im;
	}

	/**
	 * set ChatColor for positive messages
	 * @param pos
	 */
	public static void setPos(ChatColor pos) {
		GUIManager.pos = pos;
	}

	/**
	 * set ChatColor for negative messages
	 * @param neg
	 */
	public void setNeg(ChatColor neg) {
		GUIManager.neg = neg;
	}
	
	/**
	 * set ChatColor for neither positive nor negative (moderate) messages
	 * @param mod
	 */
	public void setMod(ChatColor mod) {
		GUIManager.mod = mod;
	}

	private static String im(String message)
	{
		return im + message;
	}
	
	private static String pos(String message)
	{
		return pos + message;
	}
	
	private static String neg(String message)
	{
		return neg + message;
	}
	
	private static String mod(String message)
	{
		return mod + message;
	}
	
	
	public static void wrapChatMessage(String message, MessageType type, Player p) 
	{
		message = type.getFormat() + message;
		p.sendMessage(message);
	}
	/**
	 * not implemented
	 * @param content
	 * @param contentHead
	 * @param type
	 * @param p
	 */
	@Deprecated
	public static void wrapHUD(Player p)
	{
		Scoreboard pboard = sbm.getNewScoreboard();
		Team team = pboard.registerNewTeam("team");
		team.setDisplayName("derpppp");
		team.addPlayer(p);
		
		p.setScoreboard(pboard);
	}
	/**
	 * not implemented
	 * @param content
	 * @param contentHead
	 * @param type
	 * @param p
	 * @param seconds
	 */
	@Deprecated
	public static void wrapHUD(String content, String contentHead, MessageType type, Player p, int seconds)
	{
		
	}
	
	public static void stopHUD(Player p)
	{
		stopLoop.put(p, true);
	}
	
	public static Map<String, Score> wrapHUD(String displayName, List<String> scores, MessageType type, Player p)
	{
		Scoreboard pboard = sbm.getNewScoreboard();
		Objective main = pboard.registerNewObjective("main", "dummy");
		
		main.setDisplaySlot(DisplaySlot.SIDEBAR);
		displayName = type.getFormat() + displayName;
		
		
		main.setDisplayName(displayName);
		
		Map<String, Score> scoreMap = new HashMap<String, Score>();
		List<Score> s = new ArrayList<Score>();
		for(String name:scores)
		{
		scoreMap.put(name, main.getScore(name));
		s.add(main.getScore(name));
		}
		
		for(Score sc: s)
		{
			sc.setScore(0);
		}
		hudLoop(s, main, p, type);
		p.setScoreboard(pboard);
		
		return scoreMap;
	}
	static final Random rand = new Random();
	
	static Map<Player, Boolean> stopLoop = new HashMap<Player, Boolean>();
	private static void hudLoop(final List<Score> scores, final Objective main, final Player p, final MessageType type)
	{
		stopLoop.put(p, false);
		new BukkitRunnable(){
			@Override
			public void run()
			{
				main.setDisplayName(type.getFormat() + ChatColor.values()[rand.nextInt(ChatColor.values().length)] + ChatColor.stripColor(main.getDisplayName()));
				if(stopLoop.get(p))
				{
					p.setScoreboard(sbm.getNewScoreboard());
					this.cancel();
				}
				
				
				
				
				
			}
		}.runTaskTimer(WAM.getInstance(), 2L,2L);
	}
	
	/**
	 * Currently this will fail to clear the bossbar if it is being used in a "loading"
	 * style. This will be alievated when the code has the implementation for it. Meaning soon.
	 * @param p
	 */
	public static void clearScreen(Player p)
	{
		p.setScoreboard(sbm.getNewScoreboard());
		BarAPI.stopPercentage(p);
		BarAPI.destroyBar(p);
	}
	public static void wrapPercentage(int percentage, Player p, String message, String endMessage, int seconds, boolean appendPercentage, boolean appendEndPercentage)
	{
		BarAPI.displayPercentageBar(message, endMessage, p, percentage, appendPercentage, appendEndPercentage);
	}
	
	public static void wrapCountdown(Player p, String message, String endMessage, int seconds, boolean loadUp)
	{
		BarAPI.displayLoadingBar(message, endMessage, p, seconds, loadUp);
	}
	
	
	

public enum MessageType
{
	POS_I(pos("") + im("")),
	NEG_I(neg("") + im("")),
	POS(pos("")),
	NEG(neg("")),
	MOD_IM(mod("") + im("")),
	MOD(mod("")),
	DEFAULT("");
	private final String f;
	MessageType(String f)
	{
		this.f = f;
		
	}
	public String getFormat() {return f;}
}

}
