package com.backspace119.whackamole.utils.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.backspace119.whackamole.WAM;
import com.backspace119.whackamole.game.Game;
import com.backspace119.whackamole.game.GamesManager;
import com.backspace119.whackamole.utils.handlers.GUIManager.MessageType;

public class CommandHandler implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLbl, String[] args) {
		
		if(isPlayer(sender))
		{
			Player player = (Player) sender;
			 if(args.length == 0){
					player.sendMessage(ChatColor.GRAY + "WAM v. " + WAM.getInstance().getDescription().getVersion());
					
				}else if(args[0].equalsIgnoreCase("create"))
				{
					if(player.hasPermission("wam.create"))
					{
				if(args.length == 1)
				{
					player.sendMessage("You need to input a name after create, like so: /wam create <name> |:FOLLOWING ARE OPTIONAL:| <cost> <seconds>");
					return false;
				}
				if(args.length < 3)
				{
					player.sendMessage(makeGame(player.getLocation(), args[1], 0, 0, true));
					return true;
				}else if (args.length < 4){
					if(Double.valueOf(args[2]) != null)
					{
						player.sendMessage(makeGame(player.getLocation(), args[1], Double.valueOf(args[2]), 30 , true));
					}
				}else if(args.length < 5)
				{
					if(Double.valueOf(args[2]) != null && Double.valueOf(args[3]) != null)
					{
						player.sendMessage(makeGame(player.getLocation(),args[1], Double.valueOf(args[2]), Double.valueOf(args[3]), true));
					}
				}else if(args.length < 6)
				{
					if(args[4].equalsIgnoreCase("-h"))
					{
						player.sendMessage(makeGame(player.getLocation(),args[1], Double.valueOf(args[2]), Double.valueOf(args[3]), false));
						GUIManager.wrapChatMessage("To add heads to this game type: /wam addhead <game> <playerName> <pointValue> <headRarity>", MessageType.POS, player);
					}
				}
				return true;
					}else{

						player.sendMessage(ChatColor.RED + "You do not have permission to run that command.");
						return false;
					}
			}else if(args[0].equalsIgnoreCase("highscores"))
			{
				if(args.length < 2)
				{
					for(String name: HighscoresHandler.getPlayerMap().keySet())
					{
						player.sendMessage(ChatColor.GREEN + name);
						player.sendMessage(ChatColor.GRAY + "  " + HighscoresHandler.getPlayerMap().get(name).get(0));
						player.sendMessage(ChatColor.YELLOW + "  " + HighscoresHandler.getHighscoreMap().get(name).get(0));
					}
				}else{
					if(HighscoresHandler.getHighscoresForGame(args[1]) != null)
					{
						player.sendMessage(ChatColor.GOLD + "Highscore Board for " + args[1]);
						int index = 0;
						for(String name: HighscoresHandler.getHighscorersForGame(args[1]))
						{
							player.sendMessage(ChatColor.GREEN + "" + HighscoresHandler.getHighscoresForGame(args[1]).get(index));
							player.sendMessage(ChatColor.GRAY + "  earned by: " + name);
							index++;
						}
					}else{
						player.sendMessage(ChatColor.RED + "No game with that name.");
					}
				}
				return true;
				
			}else if(args[0].equalsIgnoreCase("reload"))
			{
				if(player.hasPermission("wam.reload"))
				{
				ConfigHandler.reloadConfigs();
				player.sendMessage(ChatColor.GREEN + "WAM configs successfully reloaded");
				return true;
				}else{
					player.sendMessage(ChatColor.RED + "You do not have permission to run that command.");
					return false;
				}
			}else if(args[0].equalsIgnoreCase("list"))
			{
				player.sendMessage("Game List for WAM:");
				for(String game:GamesManager.getGamesFile().getKeys(false))
				{
				player.sendMessage(ChatColor.GREEN + game);
				}
			}else if(args[0].equalsIgnoreCase("addhead")){
			
			if(args.length > 4)
			{
				if(GamesManager.getGame(args[1]) != null)
				{
					try{
					GamesManager.getGame(args[1]).addHead(args[2], Integer.valueOf(args[3]), Integer.valueOf(args[4]));
					GamesManager.saveHeads(args[1]);
					} catch(Exception e) {
						GUIManager.wrapChatMessage("One of you're inputs for rarity or points was not an integer. (whole number)", MessageType.NEG, player);
						e.printStackTrace();
					}
				}
			}
		}
			
		}else{
			sender.sendMessage(ChatColor.GOLD + "Commands can only be sent by the player at this time.");
			sender.sendMessage(ChatColor.GREEN + "Running WAM v. " + ChatColor.YELLOW + "" + WAM.getInstance().getDescription().getVersion());
			return true;
		}
		
		return false;
	}
	
	
	private static String makeGame(Location l, String name, double cost, double seconds, boolean woolGame)
	{
		HighscoresHandler.newHighscoreList(name);
		if(cost == 0)
		{
			cost = 5;
		}
		if(seconds == 0)
		{
			seconds = 30;
		}
		
		if(woolGame)
		{
		new Game(name, GamesManager.prepareMoles(l),l.getWorld().getName(),seconds, cost, woolGame);
		}else{
			new Game(name, GamesManager.prepareMoles(l), l.getWorld().getName(), seconds, cost, woolGame, new ArrayList<String>(), new HashMap<String, Integer>(), new HashMap<String, Integer>());
		}
		GamesManager.saveGame(l, name, cost, seconds, 0);
		return "You have successfully created the whack a mole game: " + name + " make a sign with [WAM] on the first line and that name on the second";
		
	}
	
	private static boolean isPlayer(CommandSender sender)
	{
		if(sender instanceof Player) return true;
		return false;
	}

}
