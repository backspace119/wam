package com.backspace119.whackamole.utils.exceptions;

public class MalformedConfigException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public MalformedConfigException(String message)
	{
		super(message);
	}
	
	public MalformedConfigException(String message, Throwable cause)
	{
		super(message, cause);
	}
	
	public MalformedConfigException(Throwable cause)
	{
		super(cause);
	}

}
