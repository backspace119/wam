package com.backspace119.whackamole.events;


import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Wool;

import com.backspace119.whackamole.game.Game;
import com.backspace119.whackamole.game.GamesManager;
import com.backspace119.whackamole.utils.handlers.HighscoresHandler;


public class PlayerEvents implements Listener{

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event)
	{
		if(event.getBlock().getType().equals(Material.SIGN) || event.getBlock().getType().equals(Material.SIGN_POST))
		{
			Sign sign = (Sign) event.getBlock().getState();
			if(GamesManager.getGame(sign.getLine(1)) != null)
			{
				GamesManager.removeHighscoreSignForGame(sign.getLine(1), sign.getLocation());
			}
		}
	}
	@EventHandler (priority = EventPriority.HIGHEST)
	public void onBlockInteract(PlayerInteractEvent event) {

		if(event.getAction().equals(Action.LEFT_CLICK_BLOCK))
		{
			
		if(GamesManager.isPlaying(event.getPlayer().getUniqueId()))
		{
			
			
		
			if(event.getClickedBlock().getType().equals(Material.WOOL) )
			{
				
				Wool wool = (Wool) event.getClickedBlock().getState().getData();
				GamesManager.getGameByPlayer(event.getPlayer().getUniqueId()).woolClicked(wool, event.getClickedBlock());
			}else if(event.getClickedBlock().getType().equals(Material.SKULL))
			{
				Skull skull = (Skull) event.getClickedBlock().getState();
				GamesManager.getGameByPlayer(event.getPlayer().getUniqueId()).headClicked(skull);
			}
		}else if(event.getClickedBlock().getType().equals(Material.SIGN) || event.getClickedBlock().getType().equals(Material.SIGN_POST)){
			Sign sign = (Sign) event.getClickedBlock().getState();
			if(GamesManager.getHighscoreSignMap().containsKey(sign.getLine(1)) && sign.getLine(2).equalsIgnoreCase(ChatColor.GOLD + "highscore:"))
			{ 
				event.getPlayer().sendMessage(ChatColor.GREEN + "Highscore: " + HighscoresHandler.getHighscore(sign.getLine(1)));
				event.getPlayer().sendMessage(ChatColor.GRAY + "   earned by:" + HighscoresHandler.getHighscorer(sign.getLine(1)));
				event.getPlayer().sendMessage(ChatColor.GOLD + "Jackpot: " + GamesManager.getGame(sign.getLine(1)).getJackpot());
				event.getPlayer().sendMessage("Time: " + GamesManager.getGame(sign.getLine(1)).getTime() + " seconds");
			}
		}
		}else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			
			Block b = event.getClickedBlock();
			for(String s: GamesManager.getGamesFile().getKeys(false))
			{
				Game g = GamesManager.getGame(s);
			if(g.hasMole(b.getLocation()))
			{
				event.setCancelled(true);
				return;
			}
			}
			if (b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN_POST) {
				if(!GamesManager.isPlaying(event.getPlayer().getUniqueId()))
				{
				Sign sign = (Sign) b.getState();
				String[] lines = sign.getLines();
				if (lines[0].equals(ChatColor.GOLD + "[WAM]")) {
					 if (GamesManager.getSignsForGame(lines[3]) != null) {
						if (!GamesManager.getSignsForGame(lines[3]).contains(sign.getLocation())) {
							List<Location> signs = GamesManager.getSignsForGame(lines[3]);
							signs.add(sign.getLocation());
							GamesManager.setSignsForGame(lines[3], signs);
						}
					} else {
						List<Location> signs = new ArrayList<Location>();
						signs.add(sign.getLocation());
						GamesManager.setSignsForGame(lines[3], signs);
					}
					/*for(Location l: Utils.getSignsForGame(lines[2]))
					{
						System.out.println("location shit: " + l.getX() + " " + l.getY() + " " + l.getZ() + " ");
					}*/

					 if(!GamesManager.getGame(lines[3]).isRunning())
					 {
					GamesManager.getGame(lines[3]).joinGame(event.getPlayer().getUniqueId());
					 }else{
						 event.getPlayer().sendMessage(ChatColor.GOLD + "Please wait your turn to whack the moles.");
					 }
				}
			}else{
				event.getPlayer().sendMessage(ChatColor.RED + "You're already playing a game!");
			} 
		}
		}
		

	}
	
	@EventHandler
	public void onSignChange(SignChangeEvent event) {
		if (event.getLine(0).equals("[WAM]")) {
			
				if (event.getLine(1).equals("") || event.getLine(1) == null) {
					event.getBlock().breakNaturally();
					event.getPlayer().sendMessage(ChatColor.RED + "Please specify game arena name on line 2 of the sign");
				} else if (GamesManager.getGame(event.getLine(1)) != null) {

					if(event.getLine(2).equals("") || event.getLine(2) == null)
					{
						List<Location> signs;
						if (GamesManager.getSignsForGame(event.getLine(1)) != null) {
							signs = GamesManager.getSignsForGame(event.getLine(1));
						} else {

							signs = new ArrayList<Location>();
						}

						signs.add(event.getBlock().getLocation());
						GamesManager.setSignsForGame(event.getLine(1), signs);

						event.setLine(0, ChatColor.GOLD + "[WAM]");
						event.setLine(3, event.getLine(1));
						event.setLine(1, ChatColor.GREEN + "Jackpot: " + GamesManager.getGame(event.getLine(3)).getJackpot());
						event.setLine(2, ChatColor.BLACK + "Cost: " + GamesManager.getGame(event.getLine(3)).getCost());
						// Utils.getSignMap().put(event.getBlock(),
						// Utils.getGameMap().get(event.getLine(2)));
					}else{
						if(event.getLine(2).equalsIgnoreCase("-h"))
						{
							GamesManager.newHighscoreSign(event.getLine(1), event.getBlock().getLocation());
							event.setLine(2, ChatColor.GOLD + "HIGHSCORE:");
							event.setLine(3, ChatColor.GREEN + "" + HighscoresHandler.getHighscore(event.getLine(1)));
						}
					}
				} else {
					event.getBlock().breakNaturally();
					event.getPlayer().sendMessage(ChatColor.RED + "No such game exists");
				}

			}
		}
	

}

